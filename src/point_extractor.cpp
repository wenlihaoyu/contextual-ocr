#include <ros/ros.h>
#include "PointExtractor.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "PointExtractor") ;

  ros::NodeHandle nh ;
  PointExtractor ptExtractor(nh) ;
  
  ros::spin();
  
  return 0;
}
