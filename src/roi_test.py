#! /usr/bin/python

"""
roi_test.py - Testing out text detection
Jacob Beck - beckjac@oregonstate.edu
"""

import cv2
import numpy as np

img = cv2.imread('sign.jpg');
vis = img.copy()
img = cv2.cvtColor(vis, cv2.COLOR_BGR2GRAY)

# Preprocess
blur = cv2.GaussianBlur(img,(9,9),0)
cv2.imshow('blur', img)

# Find MSER
mser = cv2.MSER()
regions = mser.detect(img, None)

# Extract hulls of regions
hulls = [cv2.convexHull(r) for r in regions]
raw = [h.reshape(-1, 2) for h in hulls] # Make indexing less stupid

# Filter properties
ellipses = []
for r in raw:
    if len(r) >= 5:
        ellipses.append(cv2.fitEllipse(r)) # center, axes, angle

median_size = np.median([np.linalg.norm(e[1]) for e in ellipses])

filtered = []
for e in ellipses:
    center, axes, angle = e[:]
    
    # Reject regions that are too small
    if axes[0] < 8:
        continue
    
    # Reject regions that are too skinny
    aspect_ratio = axes[0]/axes[1] # minor/major
    if aspect_ratio < 0.1:
        continue
        
    # Reject regions that are too big
    if np.linalg.norm(axes) > median_size*1.5:
        continue
    
    # Add regions that pass
    filtered.append(e)

# Create binary image to find bounding boxes
bin_img = np.zeros(img.shape[:2], dtype=np.uint8)
for f in filtered:
    center, axes, angle = f[:]
    center = tuple([int(round(x)) for x in center])
    axes = tuple([int(round(x)) for x in axes])
    angle = int(round(angle))
    
    cv2.ellipse(bin_img, center, axes, angle, 0, 360, (255, 255, 255), -1)

cv2.imshow('bin', bin_img)

# Find bounding boxes
contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
boxes = []
for c in contours:
    boxes.append(cv2.boundingRect(c))

# Remove redundant boxes
boxes.sort(key=lambda x: x[2]*x[3], reverse=True)

final = []
for b in boxes:
    center = (b[0]+b[2]/2, b[1]+b[3]/2)
    
    contained = False
    for f in final:
        left = f[0]
        right = left + f[2]
        up = f[1]
        down = up + f[3]
        
        if (center[0] >= left and center[0] <= right) and (center[1] >= up and center[1] <= down):
            contained = True
            break
    
    if not contained:
        final.append(b)

# Draw results
for f in final:
    pt1 = (f[0], f[1])
    pt2 = (f[0]+f[2], f[1]+f[3])
    cv2.rectangle(vis, pt1, pt2, (0,255,0))

cv2.imshow('final', vis)
cv2.waitKey(0)
cv2.destroyAllWindows()
