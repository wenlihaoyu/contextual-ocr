#! /usr/bin/python

"""
find_text.py - Node to locate and extract text from kinect data
Jacob Beck - beckjac@oregonstate.edu
"""

# Standard modules
from PIL import Image as PyImage
import random
import math

# ROS modules
import rospy
from cv_bridge import CvBridge
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import Image

# 3rd party modules
import cv2
import numpy as np
import pyocr
import pyocr.builders

# Custom modules
from contextual_ocr.msg import Index
from contextual_ocr.srv import RequestPoints

class TextExtractor:
    def __init__(self, name='text'):
        rospy.init_node(name)
        
        self.rectify = rospy.get_param('apply_rectification')
        
        self.bridge = CvBridge()
        self.ocr = pyocr.get_available_tools()[0]
        
        img_sub = rospy.Subscriber('/camera/rgb/image_rect_color', Image,
            self.kinect_callback, queue_size=1, buff_size=10**7) # 10MB
        
        self.points_srv = rospy.ServiceProxy('request_points', RequestPoints)
        
        return
    
    # Callback for kinect data
    def kinect_callback(self, image):
        # Convert image to openCV format
        img = self.bridge.imgmsg_to_cv2(image)
        bw_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # Localize text
        boxes = self._get_bounding_boxes(bw_img)
        
        # Extract from image
        rois = self._extract_regions(img, boxes)
        
        # Read text
        self._run_ocr(img, rois)
        
        return
    
    # Finds bounding boxes around Regions of Interest
    def _get_bounding_boxes(self, img):
        # Preprocess
        img = cv2.GaussianBlur(img,(9,9),0)

        # Find MSER
        mser = cv2.MSER()
        regions = mser.detect(img, None)

        # Extract hulls of regions
        hulls = [cv2.convexHull(r) for r in regions]
        raw = [h.reshape(-1, 2) for h in hulls] # Make indexing less stupid

        # Filter properties
        ellipses = []
        for r in raw:
            if len(r) >= 5:
                ellipses.append(cv2.fitEllipse(r)) # center, axes, angle

        median_size = np.median([np.linalg.norm(e[1]) for e in ellipses])

        filtered = []
        for e in ellipses:
            center, axes, angle = e[:]
            
            # Reject regions that are too small
            if axes[0] < 8:
                continue
            
            # Reject regions that are too skinny
            aspect_ratio = axes[0]/axes[1] # minor/major
            if aspect_ratio < 0.2:
                continue
                
            # Reject regions that are too big
            if np.linalg.norm(axes) > median_size*1.5:
                continue
            
            # Add regions that pass
            filtered.append(e)

        # Create binary image to find bounding boxes
        bin_img = np.zeros(img.shape[:2], dtype=np.uint8)
        
        for f in filtered:
            center, axes, angle = f[:]
            center = tuple([int(round(x)) for x in center])
            axes = tuple([int(round(x)) for x in axes])
            angle = int(round(angle))
            
            cv2.ellipse(bin_img, center, axes, angle, 0, 360, (255, 255, 255), -1)
        cv2.imshow('bin', bin_img)
        # Find bounding boxes
        contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        boxes = []
        for c in contours:
            boxes.append(cv2.boundingRect(c))

        # Remove redundant boxes
        boxes.sort(key=lambda x: x[2]*x[3], reverse=True)

        final = []
        for b in boxes:
            center = (b[0]+b[2]/2, b[1]+b[3]/2)
            
            contained = False
            for f in final:
                left = f[0]
                right = left + f[2]
                up = f[1]
                down = up + f[3]
                
                if (center[0] >= left and center[0] <= right) and (center[1] >= up and center[1] <= down):
                    contained = True
                    break
            
            if not contained:
                final.append(b)
        
        return final
    
    # Gets sections of the image specified by bounding boxes
    def _extract_regions(self, img, bounding_boxes):
        regions = []
        
        # Iterate through ROI
        for bbox in bounding_boxes:
            pt1 = (bbox[0], bbox[1])
            pt2 = (bbox[0]+bbox[2], bbox[1]+bbox[3])
            
            # Extract sub-image
            local_img = img[pt1[1]:pt2[1], pt1[0]:pt2[0]]
            
            if self.rectify:
                local_img = self._rectify(local_img, pt1, pt2)
            
            regions.append([local_img, pt1, pt2])
        
        return regions
    
    # Uses point data to normalize image in bounding boxes
    def _rectify(self, img, pt1, pt2):
        # Get corresponding points
        indices = []
        for u in xrange(pt1[0], pt2[0]):
            for v in xrange(pt1[1], pt2[1]):
                indices.append(Index(u, v))
        
        rospy.loginfo("Requesting {} points".format(len(indices)))
        
        try:
            rospy.wait_for_service('request_points')
            response = self.points_srv(indices)
            points = response.points
        except rospy.ServiceException:
            rospy.logerr("TextExtractor failed to get points.")
            return img
        
        # Check number of points
        data = np.array([[pt.x, pt.y, pt.z] for pt in points])
        data = np.array([[pt.x for pt in points], [pt.y for pt in points], [pt.z for pt in points]])
        data = data[:, ~np.isnan(data).any(axis=0)] # Remove NaN
        
        if data.shape[1] < 3:
            rospy.loginfo("Got too few valid points")
            return img
        
        rospy.loginfo("Got {} valid points".format(data.shape[1]))
        
        # Thin cloud if necessary
        if data.shape[1] > 1000:
            keep = np.random.choice(np.arange(0, data.shape[1]), replace=False, size=(1000,))
            data = data[:,keep]
        
        # Condition points
        centroid = data.mean(axis=1)
        offset_pts = np.array(data - centroid[:,np.newaxis], dtype=np.float32) # float32 to reduce size
        
        try:
            rospy.loginfo("Performing SVD")
            u, s, v = np.linalg.svd(offset_pts)
        except np.linalg.LinAlgError as e:
            rospy.logerr("SVD error: {}".format(e))
            return img
        
        # Calculate R^2
        ss_tot = sum(np.linalg.norm(offset_pts, axis=0)**2)
        ss_res = s[2]**3
        r2 = (1. - ss_res/ss_tot)
        
        if r2 < 0.8:
            # Not a plane
            return img
        
        # Transform image
        normal = u[:,2] # Third column
        #~ normal = np.array([0., 0.01, 1.])
        normal /= np.linalg.norm(normal)
        centroid = np.array([0., 0., 1.])
        if normal[2] < 0:
            normal *= -1.
        
        w = img.shape[1]
        h = img.shape[0]
        
        # Get rotation to new viewpoint
        Z = np.array([0.,0.,1.])
        axis = np.cross(normal, Z)
        theta = math.acos(normal.dot(Z))
        
        R = self._matrix_from_axis_angle(axis, theta)
        
        # Find distance to plane
        #~ d = abs(normal.dot(centroid))
        d = 0.5
        
        # Get translation to new viewpoint
        #~ t = -d*normal + centroid
        t = np.array([0.,0.,0.])
        
        # Homography
        hom = R.dot(np.eye(3) + np.outer(t, normal.T)/d)
        #~ import pdb; pdb.set_trace()
        
        new_size = (int(1.5*w), int(1.5*h))
        
        warped_img = cv2.warpPerspective(img, hom, new_size,
            flags=cv2.INTER_LINEAR|cv2.WARP_INVERSE_MAP)
        
        print "trans :", t
        print "dist: ", d
        cv2.imshow("warped", warped_img)
        cv2.waitKey(3000)
        
        return warped_img
    
    # Get a rotation matrix from a given axis-angle
    def _matrix_from_axis_angle(self, axis, theta):
        axis = np.asarray(axis)
        axis = axis/math.sqrt(np.dot(axis, axis))
        a = math.cos(theta/2.0)
        b, c, d = -axis*math.sin(theta/2.0)
        aa, bb, cc, dd = a*a, b*b, c*c, d*d
        bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
        return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                         [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                         [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
    
    # Recognizes text in images
    def _run_ocr(self, vis, rois):
        # Iterate through images
        for region in rois:
            img = region[0]
            start = region[1]
            end = region[2]
            
            # Convert to PIL
            pil_img = PyImage.fromarray(img)

            # Find text
            word_boxes = self.ocr.image_to_string(
                pil_img,
                lang='eng',
                builder=pyocr.builders.WordBoxBuilder()
            )

            # Show located text
            for wbox in word_boxes:
                word = None
                try:
                    word = str(wbox.content)
                except UnicodeEncodeError:
                    word = "(Non ASCII)"
                
                if word.isspace():
                    continue
                
                print "Found: ", word
                
                # Convert from local coordinates
                pt1 = (wbox.position[0][0] + start[0], wbox.position[0][1] + start[1])
                pt2 = (wbox.position[1][0] + start[0], wbox.position[1][1] + start[1])
                
                # Annote image
                cv2.rectangle(vis, pt1, pt2, (0,255,0))
                cv2.putText(vis, word, pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0))

        cv2.imshow("result", vis)
        if cv2.waitKey(50) == 27:
            exit()
        
        return
    
    # Keep node alive
    def run(self):
        rospy.spin()
        return

if __name__ == '__main__':
    node = TextExtractor()
    node.run()

