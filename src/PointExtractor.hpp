/* 
 * pointExtractor.hpp - Class definition of a service to return points
 * at a given index from a depth image
 * Jacob Beck - jacob.beck27@gmail.com
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Point.h>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "contextual_ocr/RequestPoints.h"
#include "contextual_ocr/Index.h"

using namespace contextual_ocr;
using namespace pcl;

class PointExtractor
{
public:
    // Constructor
    PointExtractor(ros::NodeHandle);
    
private:
    // Members
    PointCloud<PointXYZ> cloud;
    bool pcReady;
    
    // Point cloud subscriber
    ros::Subscriber ptSub;
    
    // Service object
    ros::ServiceServer service;
    
    // Methods
    // Pointcloud callback
    void pointCallback(const sensor_msgs::PointCloud2&);
    
    // Service method
    bool getPoints(RequestPoints::Request&, RequestPoints::Response&);
};

// Constructor
PointExtractor::PointExtractor(ros::NodeHandle nh)
{
    pcReady = false;
    
    ptSub = nh.subscribe("/camera/depth_registered/points", 1,
        &PointExtractor::pointCallback, this);
    
    service = nh.advertiseService("request_points",
        &PointExtractor::getPoints, this);
}

// Stores ROS pointcloud as a PCL type
void PointExtractor::pointCallback(const sensor_msgs::PointCloud2 &msg)
{
    pcReady = false;
    
    PCLPointCloud2 pcl_pc;
    pcl_conversions::toPCL(msg, pcl_pc);
    fromPCLPointCloud2(pcl_pc, cloud);
    
    pcReady = true;
}

// Gets a set of points from the PCL cloud
bool PointExtractor::getPoints(RequestPoints::Request &req,
    RequestPoints::Response &res)
{
    // Wait for pointcloud to arrive
    if(!pcReady)
        return false;
    
    for(int i = 0; i < req.indices.size(); i++)
    {
        Index index = req.indices[i];
        
        PointXYZ pcl_pt;
        pcl_pt = cloud.at(index.u, index.v);
        
        geometry_msgs::Point pt;
        pt.x = pcl_pt.x;
        pt.y = pcl_pt.y;
        pt.z = pcl_pt.z;
        
        res.points.push_back(pt);
    }
    
    return true;
}
