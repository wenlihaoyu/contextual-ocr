#! /usr/bin/python

"""
ocr_test.py - Testing out tesseract
Jacob Beck - beckjac@oregonstate.edu
"""

from PIL import Image
import sys

import cv2

import pyocr
import pyocr.builders

# Get OCR info
tools = pyocr.get_available_tools()
if len(tools) == 0:
    print("No OCR tool found")
    sys.exit(1)
# The tools are returned in the recommended order of usage
tool = tools[0]
print("Will use tool '%s'" % (tool.get_name()))

langs = tool.get_available_languages()
print("Available languages: %s" % ", ".join(langs))
lang = langs[0]
print("Will use lang '%s'" % (lang))
# Ex: Will use lang 'fra'
# Note that languages are NOT sorted in any way. Please refer
# to the system locale settings for the default language
# to use.

# Capture image
camera = cv2.VideoCapture(0)

while True:
    stat, img = camera.read()

    # Condition image
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    stat, bin_img = cv2.threshold(gray_img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    # Convert to PIL
    pil_img = Image.fromarray(bin_img)

    # Find text
    word_boxes = tool.image_to_string(
        pil_img,
        lang=lang,
        builder=pyocr.builders.WordBoxBuilder()
    )

    # Show located text
    for box in word_boxes:
        word = None
        try:
            word = str(box.content)
        except UnicodeEncodeError:
            word = "(Non ASCII)"
            
        cv2.rectangle(img, box.position[0], box.position[1], (0,255,0))
        cv2.putText(img, word, box.position[0], cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0))

    cv2.imshow("thresh", bin_img)
    cv2.imshow("result", img)
    
    if cv2.waitKey(1) == 27: 
        break # esc to quit

